import { EventEmitter } from 'events'
import irc from 'irc'

function setupClient(nick, opts) {
  console.log('Building client', opts)
  const client = new irc.Client(opts.server, nick, {
    port: opts.port || '6697',
    debug: opts.debug || false,
    secure: opts.secure || true,
    channels: opts.channels || [],
    userName: opts.userName || 'Botbotbot',
    realName: opts.realName || 'Beep beep',
  })
  return client
}

const mentionRegex = /^([0-9a-zA-Z\w]+)\s?(.*)/
const commandRegex = /^\.([0-9a-zA-Z]+)\s?(.*)/


export class Message {
  constructor(bot, message) {
    this.bot = bot
    this.message = message
    this.msgType = this.getType()
    if (this.msgType === 'command') {
      let matches = this.message.text.match(commandRegex)
      this.command = matches[1]
      this.args = matches[2]
    } else if (this.msgType === 'private') {
      let matches = this.message.text.match(mentionRegex)
      this.command = matches[1]
      this.args = matches[2]
    } else if (this.msgType === 'mention') {
      let matches = this.message.text.match(mentionRegex)
      this.mention = matches[2]
      this.args = matches[2]
    }
  }

  getType() {
    const msgText = this.message.text
    if (this.message.to.toLowerCase() === this.bot.nick.toLowerCase()) {
      return 'private'
    } else if (this.message.to.indexOf('#') === 0) {
      if (msgText.toLowerCase().indexOf(this.bot.nick.toLowerCase()) === 0) {
        return 'mention'
      } else if (msgText.indexOf('.') === 0) {
        return 'command'
      }
      return 'channel'
    }
  }

  get text() {
    return this.message.text
  }

  get from() {
    return this.message.from
  }

  get to() {
    return this.message.to
  }

  test(matcher) {
    let matchAgainst = this.command || this.mention || this.message.text
    if (matcher instanceof RegExp) {
      return matcher.test(matchAgainst)
    } else if (typeof matcher ===  'string') {
      return matchAgainst.indexOf(matcher) === 0
    }
    return false
  }

  say(msg) {
    if (this.msgType === 'private') {
      return this.bot.say(this.message.from, msg)
    }
    this.bot.say(this.message.to, msg)
  }

  // Forces the response back to the user as a private message
  reply(msg) {
    return this.bot.say(this.message.from, msg)
  }


  action(msg) {
    this.bot.action(this.message.to, msg)
  }

}

export default class Bot {
  constructor(nick) {
    this.nick = nick
    this.help = {
      commands: []
    }
    this.listeners = {
      channel: [],
      private: [],
      mention: [],
      command: [],
    }
  }

  connect(settings) {
    this.client = setupClient(this.nick, settings)
    this.setupListeners()
  }

  setupListeners() {
    this.client.addListener('message', (from, to, message) => {
      console.log(from, to, message)
      let msg = new Message(this, {from: from, to: to, text: message.trim()})
      this.emit(msg)
    })
    this.client.addListener('error', function(message) {
      console.log('error: ', message);
    })
  }

  say(to, msg) {
    if (this.client) {
      this.client.say(to, msg)
    } else {
      throw new Error('IRC Client connection not setup')
    }
  }

  action(to, msg) {
    if (this.client) {
      this.client.action(to, msg)
    } else {
      throw new Error('IRC Client connection not setup')
    }
  }

  emit(msg) {
    let listeners = this.listeners[msg.msgType]
    let promises = []
    for (let i=0; i < listeners.length; i++) {
      let listener = listeners[i]
      if (msg.test(listener.test)) {
        try {
          promises.push(listener.fn.call(this, msg))
        } catch(e) {
          console.log(e)
          msg.say('I encountered an error')
        }
      }
    }
    return Promise.all(promises)
  }

  uponHearing(test, fn){
    this.listeners.channel.push({
      test: test, fn: fn
    })
  }

  uponMention(test, fn){
    this.listeners.mention.push({
      test: test, fn: fn
    })
  }

  uponCommand(test, fn){
    this.listeners.command.push({
      test: test, fn: fn
    })
  }

  uponPrivateMessage(test, fn){
    this.listeners.private.push({
      test: test, fn: fn
    })
  }

  commandHelp(cmd, help){
    this.help.commands.push({cmd, help})
  }

}


