import assert from 'assert'
import {Message} from '../bot'
import TestBot from './testbot'

let bot = new TestBot()

describe('The message object', function() {

  it('should recognize commands', async function() {
    let msg = new Message(bot, {text: '.eat fox brothers', from: 'user', to: '#channel'})
    assert.equal(msg.getType(), 'command')
    assert.equal(msg.command, 'eat')
    assert.equal(msg.args, 'fox brothers')
  });

  it('should recognize mention', async function() {
    let msg = new Message(bot, {text: 'tesTbot i hate you', from: 'user', to: '#channel'})
    assert.equal(msg.getType(), 'mention')
    assert.equal(msg.mention, 'i hate you')
    assert.equal(msg.args, 'i hate you')
  });

  it('should listen for anything', async function() {
    let msg = new Message(bot, {text: 'when is google fiber getting here', from: 'user', to: '#channel'})
    assert.equal(msg.getType(), 'channel')
  });

  it('should listen for private messages', async function() {
    let msg = new Message(bot, {text: 'when is google fiber getting here', from: 'user', to: 'testBot'})
    assert.equal(msg.getType(), 'private')
  });

});
