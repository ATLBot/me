import Bot from '../bot'
import {Message} from '../bot'


export default class TestBot extends Bot {
  constructor() {
    super('testbot')
    this.messages = []
    this.actions = []
  }

  say(to, text) {
    this.messages.push({to, text})
  }

  action(to, text) {
    this.actions.push({to, text})
  }

  async test(to, from, text) {
    await this.emit(new Message(this, {to, from, text}))
  }

}
