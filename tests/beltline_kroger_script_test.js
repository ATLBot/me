import assert from 'assert'
import beltlineKroger from '../scripts/beltline_kroger_script.js'
import TestBot from './testbot'

let bot = new TestBot()

describe('Beltline kroger script', function() {

  it('should listen for usage of beltline kroger', async function() {
    beltlineKroger(bot)
    await bot.test('#channel', 'user', 'I love beltline Kroger!!!')
    assert.equal(bot.messages[0].text, 'Correction, <user> I love Murder Kroger!!!')
  });
});
