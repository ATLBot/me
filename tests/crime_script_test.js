import assert from 'assert'
import crime, {test} from '../scripts/crime_script'
import TestBot from './testbot'

let bot = new TestBot()

describe('BodyCount', function() {

  it('should get the current crime count', async function() {
    this.timeout(10000)
    crime(bot)
    await bot.test('#channel', 'user', '.crime')
    assert.equal(bot.messages.length, 1)
    assert.ok(bot.messages[0].text.match(/Crime stats for Atlanta in the last 24/))
  })

})
