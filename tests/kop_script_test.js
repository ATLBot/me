import assert from 'assert'
import kop from '../scripts/kop_script'
import TestBot from './testbot'

let bot = new TestBot()

describe('King of Pops script', function() {

  it('should give out pops', async function() {
    kop(bot)
    await bot.test('#channel', 'user', '.kop walter_sobchak')
    assert.ok(bot.actions[0].text.match(/Hands walter_sobchak a.+/))
  });

  it('should list flavors', async function() {
    kop(bot)
    await bot.test('#channel', 'user', '.koplist')
    assert.ok(bot.messages[0].text.match(/Current Flavors:.+/))
  });
});
