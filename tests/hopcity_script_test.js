import assert from 'assert'
import hopcity from '../scripts/hopcity_script'
import TestBot from './testbot'

let bot = new TestBot()

describe('Hopcity', function() {

  it('should give someone a beer', async function() {
    this.timeout(10000) // Fuck this page is slow
    hopcity(bot)
    await bot.test('#channel', 'user', '.hopcity steve')
    assert.equal(bot.actions.length, 1)
    assert.equal(bot.messages.length, 0)
  })

})
