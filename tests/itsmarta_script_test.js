import helper from './helper'
import assert from 'assert'
import marta from '../scripts/itsmarta_script'
import TestBot from './testbot'

let bot = new TestBot()

describe('Marta info script', function() {

  it('should return the train wait time and number of trains running', async function() {
    marta(bot)
    await bot.test('#channel', 'user', '.marta')
    assert.ok(bot.messages[0].text.match(/Trains running/))
  });
});
