import assert from 'assert'
import help from '../scripts/help_script'
import TestBot from './testbot'

let bot = new TestBot()

describe('help script', function() {

  it('should give help', async function() {
    bot.commandHelp('estoria97', 'Gives <user> a coke bump')
    help(bot)
    await bot.test('testbot', 'user', 'help')
    assert.equal(bot.messages.length, 1)
    assert.ok(bot.messages[0].text.match(/Here's a list of/))
    await bot.test('testbot', 'user', 'help estoria97')
    assert.equal(bot.messages.length, 2)
    assert.ok(bot.messages[1].text.match(/Gives <user> a coke bump/))
  });
});
