import assert from 'assert'
import darksky from '../scripts/darksky_script'
import TestBot from './testbot'

let bot = new TestBot()

describe('Weather script', function() {

  it('should return the weather', async function() {
    darksky(bot)
    await bot.test('#channel', 'user', '.darksky ofw')
  });
});
