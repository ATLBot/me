import fetch from 'node-fetch'
import lodash from 'lodash'
import {load} from 'cheerio'
import moment from 'moment-timezone'
import {random, indefinite} from '../utils'
import LRU from 'lru-cache'

const cache = LRU({ max: 1, maxAge: 1000 * 60 * 60 * 24 })

async function getHappyHours() {
  let html = cache.get('drinks')
  if (!html) {
    html = await (await fetch('http://atlantabuzz.com/weekly-specials-and-things-to-do/')).text()
    cache.set('drinks', html)
  }
  const $ = load(html)
  const events = []
  const days = $('.post-content ul')
  for (let i=0; i < days.length; i++) {
    let day = $(days[i]).find('li h3')
    events.push([])
    for(let j=0; j < day.length; j++) {
      events[i].push($(day[j]).text())
    }
  }
  return events
}

export default function(bot) {
  bot.commandHelp('atldrinks', "'.atldrinks' - give back a random happy hour for today")
  bot.uponCommand('atldrinks', async function(msg){
    let day = moment().tz('America/New_York').day()
    let happyHours = await getHappyHours()
    let event = random(happyHours[day])
    return msg.say(`${msg.from}: How about ${event}`)
  })
}

