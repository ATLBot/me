import lodash from 'lodash'

function listCommdands(commands) {
  return lodash.map(commands, function(c){
    return c.cmd
  })
}

function commandHelp(commands, cmd) {
  let match = lodash.find(commands, { cmd })
  if (match) {
    return `${match.help}`
  } else {
    return `Couldn't find command: '${cmd}'`
  }

  return lodash.map(bot.commands, function(c){
    return c.cmd
  })
}

export default function(bot) {
  bot.uponPrivateMessage(/help/, function(msg){
    if (msg.args && msg.args.length > 0) {
      return msg.say(commandHelp(bot.help.commands, msg.args))
    }
    msg.say(`Here's a list of commands you can use: ${listCommdands(bot.help.commands).join(', ')}`)
  })
}
