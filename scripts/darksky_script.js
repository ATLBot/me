import fetch from 'node-fetch'
import lodash from 'lodash'

const locations = {
  'ofw': [33.767365, -84.373069, 'Old Fourth Ward'],
  'midtown': [33.783239, -84.383454, 'Midtown'],
  'l5p': [33.764226, -84.350066, 'Little 5 Point'],
  'downtown': [33.755556, -84.388518, 'Downtown'],
  'underwood': [33.808087, -84.423838, 'Underwood Hills'],
  'buckhead': [33.836680, -84.381266, 'Buckhead'],
  'decatur': [33.77558, -84.295435, 'Decatur'],
  'grantpark': [33.73453, -84.371138, 'Grant Park'],
  'gt': [33.777364, -84.397745, 'Georgia Tech'],
}

function getLocationList() {
  Object.keys(locations)
  return lodash.map(Object.keys(locations), function(key) {
    return `${locations[key][2]} (${key})`
  })
}

async function getWeather(lat, lng) {
  const req = await fetch(`https://api.forecast.io/forecast/${process.env.DARKSKY_API}/${lat},${lng}`)
  const json = await req.json()
  return json
}

export default function(bot) {
  bot.commandHelp('darksky', `'.darksky <location>' - Get the weather forcast for a location: [${getLocationList().join(', ')}]`)
  bot.uponCommand('darksky', async function(msg){
    let location = locations[msg.args.toLowerCase()]
    if (!location) {
      location = locations['downtown']
    }
    if (!process.env.DARKSKY_API) {
      return msg.say('Darksky api key needed')
    }
    let weather = await getWeather(location[0], location[1])
    msg.say(`Weather for ${location[2]}: Currently: ${weather.currently.temperature}° Next Hour: ${weather.minutely.summary} For the Day: ${weather.hourly.summary}`)
  })
}
