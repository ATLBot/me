import cron from 'cron'
import fetch from 'node-fetch'
import {load} from 'cheerio'
import {toFormData} from '../utils'
import LRU from 'lru-cache'
import pluralize from 'pluralize'

const cache = LRU({ max: 2, maxAge: 1000 * 60 * 10 })
const announceChannel = process.env.CHANNELS ? process.env.CHANNELS.split(',')[0] : null

const sections = {
  'homocide': 0,
  'rape': 1,
  'robbery': 2,
  'aggAssault': 3,
  'resBuglary': 4,
  'nonResBuglary': 5,
  'larceny': 6,
  'vehicleLarceny': 7,
  'autoTheft': 8,
}

function extractCounts($) {
  const state = {}
  const keys = Object.keys(sections)
  for (let i=0; i < keys.length; i++) {
    state[keys[i]] = $(`#MainContent_MainGridView_InnerGridView_${sections[keys[i]]} tr`).length - 1
  }
  return state
}

async function getCounts() {
  let cached = cache.get('crime')
  if (cached) {
    return cached
  }
  let body, $, req
  req = await fetch('http://opendata.atlantapd.org/Crimedata/24hrs.aspx')
  body = await req.text()
  $ = load(body)
  const form = toFormData({
    '__VIEWSTATE': $('form#ctl01 input[name=__VIEWSTATE]').val(),
    '__VIEWSTATEGENERATOR': $('form#ctl01 input[name=__VIEWSTATEGENERATOR]').val(),
    '__EVENTVALIDATION': $('form#ctl01 input[name=__EVENTVALIDATION]').val(),
    '__EVENTTARGET': 'ctl00$MainContent$Timer1',
    '__ASYNCPOST': 'true',
    'ctl00$ScriptManager1': 'ctl00$MainContent$UpdatePanel1|ctl00$MainContent$Timer1',
    '__EVENTARGUMENT': '',
  })
  req = await fetch('http://opendata.atlantapd.org/Crimedata/24hrs.aspx', {method: 'POST', body: form, headers: {
    'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',
    'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8',
    'Referer':'http://opendata.atlantapd.org/Crimedata/24hrs.aspx',
    'X-MicrosoftAjax':'Delta=true',
    'X-Requested-With':'XMLHttpRequest',
  }})
  body = await req.text()
  $ = load(body)
  let counts = extractCounts($)
  cache.set('crime', counts)
  return counts
}

export async function test(section) {
  return await getCount(section)
}

function statsMessage(counts) {
  return `Crime stats for Atlanta in the last 24 hours: Murders: ${counts.homocide}, Rapes: ${counts.rape}, Robberies: ${counts.robbery}, Aggravated Assaults: ${counts.aggAssault}, Residential Burglaries: ${counts.resBuglary}, NonResidential Burglaries: ${counts.nonResBuglary}, Larcenies: ${counts.larceny}, Vehicular Larcenies: ${counts.vehicleLarceny}, Auto Thefts: ${counts.autoTheft}`
}

export default function(bot) {
  // 9AM crime announcement
  new cron.CronJob('0 0 9 * * *', async function() {
    const counts = await getCounts()
    if (!announceChannel) {
      console.log('No channel to announce on')
      return
    }
    if (counts.homocide > 0) {
      bot.say(announceChannel, `Sadly, there ${pluralize('was', counts.homocide)} ${counts.homocide} ${pluralize('murder', counts.homocide)} in Atlanta in the last 24 hours. '.crime' for full stats`)
    } else {
      bot.say(announceChannel, `Good news everyone, there were no murders in Atlanta in the last 24 hours! '.crime' for full stats`)
    }
  }, null, true, 'America/New_York')
  bot.commandHelp('crime', '\'.crime\' - Get the crime stats for Atlanta for the last 24 hours')
  bot.uponCommand('crime', async function(msg){
    const counts = await getCounts()
    bot.say(announceChannel, statsMessage(counts))
  })
}
