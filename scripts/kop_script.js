import fetch from 'node-fetch'
import {load} from 'cheerio'
import {random} from '../utils'

const months = ['January','February','March','April','May','June','July','August','September','October','November','December']

function isAvailable(str) {
  str = str.trim()
  if (str.length === 0) return true
  const curMonth = new Date().getMonth()
  const months = str.split(' thru ')
  return months.indexOf(months[0].trim()) <= curMonth &&
         curMonth <= months.indexOf(months[1].trim())
}

async function getFlavors() {
  const req = await fetch('http://atlanta.kingofpops.com/pops/flavors')
  const $ = load(await req.text())
  const popEls = $('.pop-details')
  const pops = []
  for (let i=0; i < popEls.length; i++) {
    let availability = $(popEls[i]).find('.availability').text().trim()
    if (isAvailable(availability)) {
      pops.push($(popEls[i]).find('h5').text().trim())
    }
  }
  return pops
}

export default function(bot) {
  bot.commandHelp('kop', "'.kop <user>' - Give <user> a King of Pops popsicle")
  bot.uponCommand(/^kop$/i, async function(msg){
    let flavors = await getFlavors()
    if (msg.args.length === 0) {
      return msg.say(`Current Flavors: ${flavors.join(', ')}`)
    }
    msg.action(`Hands ${msg.args} a tasty ${random(flavors)} popsicle`)
  })
  bot.commandHelp('koplist', "'.koplist' - Get a list of the current King of Pops flavors for Atlanta")
  bot.uponCommand(/^koplist$/i, async function(msg){
    let flavors = await getFlavors()
    msg.say(`Current Flavors: ${flavors.join(', ')}`)
  })
}
