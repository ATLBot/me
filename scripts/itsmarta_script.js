import fetch from 'node-fetch'
import lodash from 'lodash'
import LRU from 'lru-cache'

const cache = LRU({ max: 2, maxAge: 1000 * 30 })

async function getTrainTimes(lat, lng) {
  if (cache.get('trains')) {
    return cache.get('trains')
  }
  const req = await fetch(`http://developer.itsmarta.com/RealtimeTrain/RestServiceNextTrain/GetRealtimeArrivals?apikey=${process.env.MARTA_API_KEY}`)
  const json = await req.json()
  cache.set('trains', json)
  return json
}

async function getTrainInfo() {
  let data = await getTrainTimes()
  let trainGroup = lodash.countBy(data, function(d) {return d['TRAIN_ID']})
  return {
    trainCount: Object.keys(trainGroup).length,
    waitTime: lodash.meanBy(data, function(d){
      let wait = d['WAITING_SECONDS']
      wait = parseInt(wait, 10)
      if (wait < 0 ) {
        return 0
      } else {
        return wait
      }
    }),
  }
}

export default function(bot) {
  bot.commandHelp('marta', `'.marta' - Gets current Marta wait times`)
  bot.uponCommand('marta', async function(msg){
    if (!process.env.MARTA_API_KEY) {
      return msg.say('MARTA api key needed')
    }
    let info = await getTrainInfo()
    msg.say(`Trains running: ${info.trainCount} - Average Wait Time: ${Math.floor(info.waitTime/60)} minutes`)
  })
}
