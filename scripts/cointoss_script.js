import {random} from '../utils'

const coin = ["heads", "tails"]

export default function(bot){
  bot.uponHearing(/(throw|flip|toss) a coin/, function(msg){
    msg.say(random(coin))
  })
}

