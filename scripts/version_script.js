export default function(bot){
  bot.uponMention(/version/, function(res){
    res.say(`https://github.com/AtlBot/me/commit/${process.env.REVISION} - ${process.env.REVISION_LOG}`)
  })
}

