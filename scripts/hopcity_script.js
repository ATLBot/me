import fetch from 'node-fetch'
import lodash from 'lodash'
import {load} from 'cheerio'
import {random, indefinite} from '../utils'
import LRU from 'lru-cache'

const cache = LRU({ max: 2, maxAge: 1000 * 60 * 60 * 8 })

async function getBeers() {
  let cached = cache.get('beer')
  if (cached) {
    return cached
  }
  const req = await fetch('http://www.hopcitybeer.com/krog-street-market')
  const $ = load(await req.text())
  const beers = []
  const growlers = $($('#growlertownrow .fl-menu-section')[0]).find('h3')
  const pints = $($('#growlertownrow .fl-menu-section')[1]).find('h3')
  const beerEls = $(growlers).add(pints)
  for (let i=0; i < beerEls.length; i++) {
    beers.push($(beerEls[i]).text().trim())
  }
  cache.set('beer', beers)
  return beers
}

export default function(bot) {
  const adj = ['delicious pint', 'tasty pint']
  bot.commandHelp('hopcity', "'.hopcity <user>' - gives a Hopcity stocked beer to <user>")
  bot.uponCommand('hopcity', async function(msg){
    let beers = await getBeers()
    let beer = random(beers)
    if (msg.args.length === 0) {
      return msg.action(`Grabs a ${random(adj)} of ${beer}`)
    }
    msg.action(`Hands ${msg.args} a ${random(adj)} of ${beer}`)
  })
}

