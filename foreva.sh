#!/bin/bash

while true; do
  echo "Forever, forever ever, forever ever?"
  git pull origin master
  export REVISION="$(git rev-parse HEAD | xargs echo -n)"
  export REVISION_LOG="$(git log --pretty=format:'%s' -n 1 | xargs echo -n)"
  node index.js
  sleep 10
done
