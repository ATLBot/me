require('dotenv').config()
require("babel-register")
require("babel-polyfill");
var Bot = require("./bot").default

var bot = new Bot(process.env.NICK)

bot.connect({
  server: process.env.SERVER,
  debug: false,
  userName: process.env.NICK,
  realName: process.env.REALNAME || "I have no real name",
  channels: process.env.CHANNELS.split(','),
})

bot.client.addListener('motd', function() {
  bot.say('nickserv', `identify ${process.env.NICKSERV_PASSWORD}`)
})

var scripts = require('require-all')({
  dirname     :  __dirname + '/scripts',
  filter      :  /(.+script)\.js$/,
  resolve     : function (script) {
    return script.default(bot)
  }
});
